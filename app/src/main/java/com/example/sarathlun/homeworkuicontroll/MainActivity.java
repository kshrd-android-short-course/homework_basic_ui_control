package com.example.sarathlun.homeworkuicontroll;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener {

    // working with name,phone,email
    EditText txtName,txtPhone,txtEmail;
    // working with gender
    String gender;
    // working with date of birth
    TextView txtDateOfBirth;
    // working with place of birth
    String[] provinces = {
            "BANTEAY MEANCHEY",
            "BATTAMBANG",
            "KAMPONG CHAM",
            "KAMPONG CHHNANG",
            "KAMPONG SPEU",
            "KAMPONG THOM",
            "KAMPOT",
            "KANDAL",
            "KOH KONG",
            "KRATIE",
            "MONDUL KIRI",
            "PHNOM PENH",
            "PREAH VIHEAR",
            "PREY VENG",
            "PURSAT",
            "RATANAK KIRI",
            "SIEM REAP",
            "PREAH SIHANOUK",
            "STUNG TRENG",
            "SVAY RIENG",
            "TAKEO",
            "OTDAR MEANCHEY",
            "KEP",
            "PAILIN"
    };
    Spinner provinceSpinner;
    // working with timePicker
    TextView txtStartTime, txtEndTime;
    // button submit
    Button btnSubmit;
    // declare global fields
    String selectedProvince;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtDateOfBirth = findViewById(R.id.txtDateOfBirth);
        provinceSpinner = findViewById(R.id.txtPlaceOfBirth);
        txtStartTime = findViewById(R.id.txtStartTime);
        txtEndTime = findViewById(R.id.txtEndTime);
        btnSubmit = findViewById(R.id.btnSubmit);
        txtName = findViewById(R.id.txtName);
        txtPhone = findViewById(R.id.txtPhone);
        txtEmail = findViewById(R.id.txtEmail);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_dropdown_item_1line,
                provinces
        );
        provinceSpinner.setAdapter(adapter);
        provinceSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedProvince = provinces[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onRadioClick(View view){
        RadioButton radioButton = (RadioButton) view;
        if (radioButton.isChecked()){
            gender = radioButton.getText().toString();
        }

    }

    List<String> shortCourses = new ArrayList<>();
    public void onCheckBoxShortCourse(View view) {
        CheckBox checkBox = (CheckBox) view;
        if (checkBox.isChecked()) {
            shortCourses.add(checkBox.getText().toString());
        } else {
            shortCourses.remove(checkBox.getText().toString());
        }
    }

    Calendar c = Calendar.getInstance();

    public void onDatePickerClick(View view) {

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, this, y, m, d);
        dialog.show();
    }



    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String m = "", d = "";
        month = month + 1;
        if (month < 10) {
            m = "0" + month;
        } else {
            m = "" + month;
        }
        if (dayOfMonth < 10) {
            d = "0" + dayOfMonth;
        } else {
            d = "" + dayOfMonth;
        }
        txtDateOfBirth.setText(d + "/" + m + "/" + year);
    }

    public void onTimePickerClick(View view) {
        int h = c.get(Calendar.HOUR);
        int m = c.get(Calendar.MINUTE);
        TimePickerDialog dialog = new TimePickerDialog(this, this, h, m, false);
        dialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String h = "", m = "";
        if (hourOfDay < 10) {
            h = "0" + hourOfDay;
        } else {
            h = "" + hourOfDay;
        }
        if (minute < 10) {
            m = "0" + minute;
        } else {
            m = "" + minute;
        }
        txtStartTime.setText(h + ":" + m);
        txtEndTime.setText(h + ":" + m); //todo: fixme!
    }

    @Override
    public void onClick(View v) {
        String fullName = txtName.getText().toString(),
                phone = txtPhone.getText().toString(),
                email = txtEmail.getText().toString(),
                gender = this.gender,
                dateOfBirth = txtDateOfBirth.getText().toString(),
                shortCourses = this.shortCourses.toString(),
                placeOfBirth = selectedProvince,
                startTime = txtStartTime.getText().toString(),
                endTime = txtEndTime.getText().toString();

        showMessage(
                fullName + "," + phone + "," + email + "," + gender + "," + dateOfBirth + "," + shortCourses + "," + placeOfBirth + "," + startTime + "," + endTime
        );

    }

    private void showMessage(String msg){
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
    }


}
